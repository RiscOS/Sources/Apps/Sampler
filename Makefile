# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Sampler
#
# ***********************************
# ***    C h a n g e   L i s t    ***
# ***********************************
# Date       Name   Description
# ----       ----   -----------
# 29-Apr-98  RML    Created

#
# Program specific options:
#
COMPONENT   = Sampler
APP         = !Sampler
RDIR        = Resources
LDIR        = ${RDIR}.${LOCALE}

#
# Generic options:
#
MKDIR   = do mkdir -p
CC      = cc
LD      = link
CP      = copy
RM      = remove
SQUISH  = squish
WIPE    = x wipe

AFLAGS  = ${THROWBACK} -depend !Depend -nocache -stamp -quit -module
CFLAGS  = ${THROWBACK} -depend !Depend -ffa ${INCLUDES} 
CPFLAGS = ~cfr~v
SQFLAGS = -nolist
WFLAGS  = ~c~v

#
# Libraries
#
CLIB       = CLib:o.stubs
EVENTLIB   = tbox:o.eventlib
TOOLBOXLIB = tbox:o.toolboxlib
WIMPLIB    = tbox:o.wimplib
WRAPPER    = RISC_OSLib:s.ModuleWrap

#
# Include files
#
INCLUDES = -Itbox:,C:


FILES =\
 ${RDIR}.!Boot \
 ${LDIR}.!Help \
 ${RDIR}.!Run \
 ${RDIR}.!Sprites22 \
 ${RDIR}.Sprites \
 ${RDIR}.!RunImage \
 ${LDIR}.Messages \
 ${LDIR}.Res

OBJS =\
  o.main \
  o.sample \
  o.common

#
# Rule patterns
#
.SUFFIXES: .o

.c.o:; @echo
	@echo Compiling $<
	@${CC} ${CFLAGS} ${DFLAGS}        -c -o $@ $<
	
#
# Main rules:
#


install: ${FILES}
	 @echo
	 @echo ${COMPONENT}: Creating application ${APP}
	 ${MKDIR} ${INSTDIR}.${APP}
	 ${CP} ${RDIR}.!Boot      ${INSTDIR}.${APP}.!Boot      ${CPFLAGS}
	 ${CP} ${RDIR}.!Run       ${INSTDIR}.${APP}.!Run       ${CPFLAGS}
	 ${CP} ${RDIR}.!Sprites22 ${INSTDIR}.${APP}.!Sprites22 ${CPFLAGS}
	 ${CP} ${RDIR}.Sprites    ${INSTDIR}.${APP}.Sprites    ${CPFLAGS}
	 ${CP} ${RDIR}.!RunImage  ${INSTDIR}.${APP}.!RunImage  ${CPFLAGS} 
	 ${CP} ${LDIR}.Messages   ${INSTDIR}.${APP}.Messages   ${CPFLAGS}
	 ${CP} ${LDIR}.Res        ${INSTDIR}.${APP}.Res        ${CPFLAGS}
	 Access ${INSTDIR}.${APP}.* wr/r
	 @echo
	 @echo ${COMPONENT}: Application installed {Disc}

clean:
	${WIPE} o.*      ${WFLAGS}
	${WIPE} ${APP}.* ${WFLAGS}
	${RM} ${RDIR}.!RunImage
	${RM} ${APP}
	@echo ${COMPONENT}: cleaned

#
# Static dependencies:
#

Resources.!RunImage: ${OBJS} ${EVENTLIB} ${TOOLBOXLIB} ${WIMPLIB} ${CLIB}
	${LD} -o $@ ${CLIB} ${EVENTLIB} ${TOOLBOXLIB} ${WIMPLIB} ${OBJS}


#---------------------------------------------------------------------------
# Dynamic dependencies:
